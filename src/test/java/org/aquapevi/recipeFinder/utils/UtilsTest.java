package org.aquapevi.recipeFinder.utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import junit.framework.Assert;
import org.aquapevi.recipeFinder.exception.NoDataFoundException;
import org.aquapevi.recipeFinder.model.Item;
import org.aquapevi.recipeFinder.model.Recipe;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Test case for Utils class
 */
public class UtilsTest {
    private Statement statement;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @Before
    public void setUp() throws Exception {
        statement = Utils.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
    }

    @After
    public void tearDown() throws Exception {
        statement.execute("DROP TABLE items;");
        statement.execute("DROP TABLE recipes;");
    }

    private int resultSetCount(ResultSet rs) {
        int rows = 0;
        try {
            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rows;
    }

    @Test
    public void testParseItemsDataFile() throws Exception {
        // Parse the file that has valid data
        File goodDataFile = new File("src/test/data/test_items1.csv");

        Utils.parseItemsDataFile(goodDataFile);

        // Get the records back from the table
        ResultSet resultSet = statement.executeQuery("SELECT name, amount, unit, useBy FROM items;");

        Assert.assertTrue("All records have not been parsed.", resultSetCount(resultSet) == 4);

        if (resultSet.next()) {
            Assert.assertNotSame("Data was not imported correctly. (bread == " + resultSet.getString("name"),
                                            "bread".equals(resultSet.getString("name")));
            Assert.assertNotSame("Data was not imported correctly. (10 == " + resultSet.getInt("amount"),
                                            resultSet.getInt("amount") == 10);
            Assert.assertNotSame("Data was not imported correctly. (slices == " + resultSet.getString("unit"),
                                            "slices".equals(resultSet.getString("unit")));
            sdf.setLenient(false);
            Assert.assertNotSame("Data was not imported correctly. (25/12/2014 == " + resultSet.getTimestamp("useBy"),
                                            resultSet.getTimestamp("useBy").getTime() == sdf.parse("25/12/2014").getTime());
        }
        resultSet.close();

        statement.execute("DELETE FROM items;");

        // Parse the file that has invalid data
        File badDataFile = new File("src/test/data/test_items2.csv");

        Utils.parseItemsDataFile(badDataFile);
        resultSet = statement.executeQuery("SELECT name, amount, unit, useBy FROM items;");

        Assert.assertFalse("All records have been parsed.", resultSetCount(resultSet) == 4);
        Assert.assertTrue("Invalid records were parsed.", resultSetCount(resultSet) == 3);
        resultSet.close();
    }

    @Test
    public void testParseJSONFile() throws Exception {
        // Parse the file that has valid data
        File goodDataFile = new File("src/test/data/test_recipes1.json");

        Utils.parseRecipesDataFile(goodDataFile);

        // Get the records back from the table
        ResultSet resultSet = statement.executeQuery("SELECT name,\n" +
                                                    "    ingredient_name,\n" +
                                                    "    ingredient_amount,\n" +
                                                    "    ingredient_unit\n" +
                                                    "FROM recipes;\n");

        Assert.assertTrue("All records have not been parsed.", resultSetCount(resultSet) == 4);

        if (resultSet.next()) {
            Assert.assertNotSame("Data was not imported correctly. (grilled cheese on toast == " + resultSet.getString("name"),
                                                "grilled cheese on toast".equals(resultSet.getString("name")));
            Assert.assertNotSame("Data was not imported correctly. (bread == " + resultSet.getString("ingredient_name"),
                                                "bread".equals(resultSet.getString("ingredient_name")));
            Assert.assertNotSame("Data was not imported correctly. (2 == " + resultSet.getInt("ingredient_amount"),
                                                resultSet.getInt("ingredient_amount") == 2);
            Assert.assertNotSame("Data was not imported correctly. (slices == " + resultSet.getString("ingredient_unit"),
                                                "slices".equals(resultSet.getString("ingredient_unit")));
        }
        resultSet.close();

        statement.execute("DELETE FROM recipes;");

        // Parse the file that has invalid data
        File badDataFile = new File("src/test/data/test_recipes2.json");

        try {
            Utils.parseRecipesDataFile(badDataFile);
        } catch (Exception e) {
            System.out.println("Exception " + e.getMessage());
        }

        resultSet = statement.executeQuery("SELECT name,\n" +
                                            "    ingredient_name,\n" +
                                            "    ingredient_amount,\n" +
                                            "    ingredient_unit\n" +
                                            "FROM recipes;\n");

        Assert.assertFalse("All records have been parsed.", resultSetCount(resultSet) == 4);
        Assert.assertTrue("Invalid records were parsed.", resultSetCount(resultSet) == 0);
        resultSet.close();
    }

    @Test
    public void testFindRecipe() throws Exception {
        // Setup test data that has a valid recipe that can be found. It also checks to make sure
        // that the recipe with the item that expires sooner is found.
        File goodDataFile = new File("src/test/data/test_items1.csv");

        Utils.parseItemsDataFile(goodDataFile);

        goodDataFile = new File("src/test/data/test_recipes1.json");

        Utils.parseRecipesDataFile(goodDataFile);

        // Find recipe
        Recipe recipe = Utils.findRecipe();

        Assert.assertTrue("Recipe could not be found for valid data.", recipe != null);
        Assert.assertTrue("Incorrect recipe has been found.", "salad sandwich".equals(recipe.getName()));

        // Test with items in fridge and recipes that will not find a recipe.
        // Reason - Valid recipe uses expired food
        statement.execute("DELETE FROM items;");
        statement.execute("DELETE FROM recipes;");

        goodDataFile = new File("src/test/data/test_items3.csv");

        Utils.parseItemsDataFile(goodDataFile);

        goodDataFile = new File("src/test/data/test_recipes3.json");

        Utils.parseRecipesDataFile(goodDataFile);

        // Find recipe
        recipe = Utils.findRecipe();

        Assert.assertTrue("Found a recipe with expired items.", "grilled cheese on toast".equals(recipe.getName()));

        // Test with items in fridge and recipes that will not find a recipe.
        // Reason - Recipes use items that are not in fridge
        statement.execute("DELETE FROM items;");
        statement.execute("DELETE FROM recipes;");

        goodDataFile = new File("src/test/data/test_items4.csv");

        Utils.parseItemsDataFile(goodDataFile);

        goodDataFile = new File("src/test/data/test_recipes4.json");

        Utils.parseRecipesDataFile(goodDataFile);

        // Find recipe
        recipe = Utils.findRecipe();

        Assert.assertTrue("Found a recipe with items not in fridge.", recipe == null);
    }

    @Test
    public void testResetDB() throws SQLException, NoDataFoundException, IOException {
        File goodDataFile = new File("src/test/data/test_items1.csv");

        Utils.parseItemsDataFile(goodDataFile);

        goodDataFile = new File("src/test/data/test_recipes1.json");

        Utils.parseRecipesDataFile(goodDataFile);

        Utils.resetDB();

        // Get the records back from the table
        ResultSet resultSet = statement.executeQuery("SELECT name, amount, unit, useBy FROM items;");

        Assert.assertTrue("All records have not been deleted from items.", resultSetCount(resultSet) == 0);

        // Get the records back from the table
        resultSet = statement.executeQuery("SELECT name,\n" +
                "    ingredient_name,\n" +
                "    ingredient_amount,\n" +
                "    ingredient_unit\n" +
                "FROM recipes;\n");

        Assert.assertTrue("All records have not been deleted from recipes.", resultSetCount(resultSet) == 0);
    }
}
