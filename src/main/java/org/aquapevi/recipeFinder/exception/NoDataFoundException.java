package org.aquapevi.recipeFinder.exception;

/**
 * Exception thrown when no data was found after parsing data from the file.
 */
public class NoDataFoundException extends Exception {
    public NoDataFoundException() {super();}

    public NoDataFoundException(String str) {super(str);}
}
