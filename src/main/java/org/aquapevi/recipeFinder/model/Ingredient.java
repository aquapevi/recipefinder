package org.aquapevi.recipeFinder.model;

import org.aquapevi.recipeFinder.utils.Unit;

/**
 * A single ingredient
 */
public class Ingredient {
    private String item;
    private int amount;
    private Unit unit;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ingredient that = (Ingredient) o;

        if (amount != that.amount) return false;
        if (!item.equals(that.item)) return false;
        if (unit != that.unit) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = item.hashCode();
        result = 31 * result + amount;
        result = 31 * result + unit.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "item='" + item + '\'' +
                ", amount=" + amount +
                ", unit=" + unit +
                '}';
    }
}
