package org.aquapevi.recipeFinder.model;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A single recipe
 */
public class Recipe {
    private String name;
    private ArrayList<Ingredient> ingredients;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public boolean isEmpty() {
        if ((name ==null || (name != null && name.trim().isEmpty()) &&
                (ingredients == null || (ingredients != null && ingredients.size() == 0)))) {
            return true;
        }

        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recipe recipe = (Recipe) o;

        if (!ingredients.equals(recipe.ingredients)) return false;
        if (!name.equals(recipe.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + ingredients.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "name='" + name + '\'' +
                ", ingredients=" + ingredients +
                '}';
    }
}
