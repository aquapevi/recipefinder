package org.aquapevi.recipeFinder.model;

import org.aquapevi.recipeFinder.utils.Unit;

import java.sql.Timestamp;
import java.util.Date;

/**
 * A single item.
 */
public class Item {
    private String item;
    private int amount;
    private Unit unit;
    private Timestamp useBy;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Timestamp getUseBy() {
        return useBy;
    }

    public void setUseBy(Timestamp useBy) {
        this.useBy = useBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item1 = (Item) o;

        if (amount != item1.amount) return false;
        if (!item.equals(item1.item)) return false;
        if (unit != item1.unit) return false;
        if (!useBy.equals(item1.useBy)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = item.hashCode();
        result = 31 * result + amount;
        result = 31 * result + unit.hashCode();
        result = 31 * result + useBy.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Item{" +
                "item='" + item + '\'' +
                ", amount=" + amount +
                ", unit=" + unit +
                ", useBy=" + useBy +
                '}';
    }
}
