package org.aquapevi.recipeFinder.utils;

/**
 * Unit used in the recipes.
 */
public enum Unit {
    of,
    grams,
    ml,
    slices
}
