package org.aquapevi.recipeFinder.utils;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aquapevi.recipeFinder.exception.NoDataFoundException;
import org.aquapevi.recipeFinder.model.Ingredient;
import org.aquapevi.recipeFinder.model.Item;
import org.aquapevi.recipeFinder.model.Recipe;

import java.io.*;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Utils class
 */
public class Utils {
    public static final Log log = LogFactory.getLog(Utils.class);
    public static final String CSV_SPLIT_BY = ",";
    public static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    public static final ObjectMapper objectMapper = new ObjectMapper();
    private static Connection connection;

    private static final Utils ref = new Utils();

    public static Connection getConnection() {
        return connection;
    }

    public static Utils getUtils() {
        return ref;
    }

    private Utils() {
        try {
            log.info("Initializing the DB...");
            // Setup the database. Stop application if this does not succeed.
            Class.forName("org.h2.Driver");

            connection = DriverManager.getConnection("jdbc:h2:mem:recipeFinder", "sa", "sa");

            Statement statement = connection.createStatement();

            statement.execute("CREATE TABLE items (\n" +
                    "  id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,\n" +
                    "  name varchar(50) NOT NULL,\n" +
                    "  amount int DEFAULT NULL,\n" +
                    "  unit varchar(20) DEFAULT NULL,\n" +
                    "  useBy datetime DEFAULT NULL\n" +
                    ")");

            statement.execute("CREATE TABLE recipes (\n" +
                    "  id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,\n" +
                    "  name varchar(100) NOT NULL,\n" +
                    "  ingredient_name varchar(50) NOT NULL,\n" +
                    "  ingredient_amount int NOT NULL,\n" +
                    "  ingredient_unit varchar(20) NOT NULL,\n" +
                    "  ingredient_count int NOT NULL\n" +
                    ")");
            statement.execute("CREATE INDEX recipes_ingredient_name_idx ON recipes(ingredient_name);");

            log.info("Done setting up DB...");
        } catch (ClassNotFoundException e) {
            log.error("H2DB driver not found. Cannot proceed - " + e.getMessage());
            throw new RuntimeException("Application Exception!");
        } catch (SQLException e) {
            log.error("Application error - " + e.getMessage());
            throw new RuntimeException("Application Exception!");
        }
    }

    /**
     * Parse CSV file with the items in fridge data. Ignores invalid data - for example, all blank lines,
     * all lines that do not have all the required fields,
     * @param itemDataFile the data file in CSV format
     * @throws IOException, FileNotFoundException is thrown in case of IO issues
     *                      and NoDataFoundException is thrown if no records were found after parsing
     */
    public static void parseItemsDataFile(File itemDataFile) throws FileNotFoundException,
                                                                    IOException,
                                                                    NoDataFoundException,
                                                                    SQLException
    {
        BufferedReader bufferedReader = null;
        String line = "";
        sdf.setLenient(false);
        int parsedRecords = 0;

        try {
            log.info("Starting to parse CSV file...");
            bufferedReader = new BufferedReader(new FileReader(itemDataFile));

            // Loop through the csv file and prepare INSERT statements that can be run as a batch at the
            // end of the file parsing.
            PreparedStatement preparedStatement = connection.prepareStatement(
                                   "INSERT INTO items\n" +
                                           "(name,\n" +
                                           "amount,\n" +
                                           "unit,\n" +
                                           "useBy)\n" +
                                           "VALUES\n" +
                                           "(?,\n" +
                                           "?,\n" +
                                           "?,\n" +
                                           "?);\n"
                                        );

            while ((line = bufferedReader.readLine()) != null) {
                if (line.trim().length() == 0) { //Blank line, ignore
                    log.debug("Ignoring blank line while parsing csv file...");
                    continue;
                } else {
                    String[] fields = line.split(CSV_SPLIT_BY);

                    // Check the line has 4 fields.
                    if (fields.length != 4) {
                        log.info("Line does not have the required fields, ignoring...");
                        continue;
                    } else {
                        // Set the fields for a new Item
                        try {
                            // Setting the values to the item object has no value here except help validating.
                            Item item = new Item();
                            item.setItem(fields[0]);
                            item.setAmount(Integer.parseInt(fields[1]));
                            item.setUnit(Unit.valueOf(fields[2]));
                            item.setUseBy(new Timestamp(sdf.parse(fields[3]).getTime()));

                            preparedStatement.setString(1, item.getItem());
                            preparedStatement.setInt(2, item.getAmount());
                            preparedStatement.setString(3, item.getUnit().name());
                            preparedStatement.setTimestamp(4, item.getUseBy());

                            preparedStatement.addBatch();

                            parsedRecords++;
                        } catch (ParseException e) {
                            log.error("Ignoring line that could not be parsed (" + line + ") - " + e.getMessage());
                        } catch (IllegalArgumentException e) {
                            log.error("Ignoring line that could not be parsed (" + line + ") - " + e.getMessage());
                        }
                    }
                }
            }

            // Throw a NoDataFoundException if no lines could be parsed.
            if (parsedRecords == 0) {
                throw new NoDataFoundException("No records were parsed from " + itemDataFile.getName());
            } else {
                // Save the records
                connection.setAutoCommit(false);
                int[] batchStatus = preparedStatement.executeBatch();
                int savedRecords = 0;

                for (int ctr : batchStatus) {
                    if (ctr > 0) {
                        savedRecords++;
                    }
                }

                if (savedRecords == 0) {
                    log.info(parsedRecords + " records were parsed but no records were saved!!");
                    throw new NoDataFoundException("No records were saved from " + itemDataFile.getName());
                }
            }
        } catch (FileNotFoundException e) {
            log.error("File not found while parsing CSV file - " + e.getMessage());
            throw e;
        } catch (IOException e) {
            log.error("IO Exception while parsing CSV file - " + e.getMessage());
            throw e;
        } catch (SQLException e) {
            log.error("SQL Exception while parsing CSV file- " + e.getMessage());
            throw e;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    log.error("IO Exception while closing reader - " + e.getMessage());
                }
            }
        }
    }

    /**
     * Parse the recipes data file and return a Json tree
     * @param recipesDataFile - recipe json data file
     * @throws JsonParseException - thrown in case of a parse exception
     * @throws IOException - thrown in case of a IO exception
     */
    public static void parseRecipesDataFile(File recipesDataFile) throws JsonParseException,
                                                                         IOException,
                                                                         NoDataFoundException,
                                                                         SQLException
    {
        int parsedRecords = 0;

        try {
            JsonFactory jsonFactory = new MappingJsonFactory();
            JsonParser jsonParser = jsonFactory.createParser(recipesDataFile);

            // Parse the json file and start a batch for all parsed records
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO recipes\n" +
                                                                            "(name,\n" +
                                                                            "ingredient_name,\n" +
                                                                            "ingredient_amount,\n" +
                                                                            "ingredient_unit,\n" +
                                                                            "ingredient_count)\n" +
                                                                            "VALUES\n" +
                                                                            "(?,\n" +
                                                                            "?,\n" +
                                                                            "?,\n" +
                                                                            "?,\n" +
                                                                            "?);\n");

            try {
                JsonToken current;

                current = jsonParser.nextToken();
                if (current != JsonToken.START_ARRAY) {
                    throw new JsonParseException("Root should be an array", JsonLocation.NA);
                }

                while ((current = jsonParser.nextToken()) != JsonToken.END_ARRAY) {
                    if (current == JsonToken.START_OBJECT) { // New Record
                        try {
                            Recipe recipe = new Recipe(); // Object to store values temporarily - will also validate
                            ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
                            current = jsonParser.nextToken(); // Move to the next field
                            String fieldName = jsonParser.getCurrentName();
                            int ingredient_ctr = 0;

                            if ("name".equals(fieldName)) {
                                // Name of the recipe
                                current = jsonParser.nextToken(); // Move to the next field
                                recipe.setName(jsonParser.getValueAsString());

                                // Now get the ingredients
                                current = jsonParser.nextToken();
                                fieldName = jsonParser.getCurrentName();

                                // If the token has to be ingredients. If it is not,
                                // move the token back and continue
                                if (!"ingredients".equals(fieldName)) {
                                    continue;
                                } else {
                                    // The ingredients as an array
                                    if ((current = jsonParser.nextToken()) == JsonToken.START_ARRAY) {
                                        ingredient_ctr = 0;
                                        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                                            // Read the ingredients as a tree
                                            JsonNode jsonNode = jsonParser.readValueAsTree();

                                            Ingredient ingredient = new Ingredient();
                                            ingredient.setItem(jsonNode.get("item").asText());
                                            ingredient.setAmount(jsonNode.get("amount").asInt());
                                            ingredient.setUnit(Unit.valueOf(jsonNode.get("unit").asText()));

                                            ingredients.add(ingredient);
                                            ingredient_ctr++;
                                        }

                                        recipe.setIngredients(ingredients);
                                    }
                                }
                            } else {
                                // Some other field, ignore and continue
                                continue;
                            }

                            // If recipes is not empty, then add to batch
                            if (!recipe.isEmpty()) {
                                // Prepare the insert statements for all the ingredients
                                for (Ingredient ingredient : recipe.getIngredients()) {
                                    preparedStatement.setString(1, recipe.getName());
                                    preparedStatement.setString(2, ingredient.getItem());
                                    preparedStatement.setInt(3, ingredient.getAmount());
                                    preparedStatement.setString(4, ingredient.getUnit().name());
                                    preparedStatement.setInt(5, ingredient_ctr);

                                    preparedStatement.addBatch();
                                    parsedRecords++;
                                }
                            }
                        } catch (JsonParseException e) {
                            log.error("Skipping Json record due to error parsing recipes data file - " + e.getMessage());
                            jsonParser.skipChildren();
                        } catch (IllegalArgumentException e) {
                            log.error("Skipping json record sue to error parsing recipe data file - " + e.getMessage());
                            jsonParser.skipChildren();
                        }
                    }
                }
            } catch (JsonParseException e) {
                log.error("Error parsing recipes data file - " + e.getMessage());
                throw e;
            }

            // Throw a NoDataFoundException if no lines could be parsed.
            if (parsedRecords == 0) {
                throw new NoDataFoundException("No records were parsed from " + recipesDataFile.getName());
            } else {
                // Save the records
                connection.setAutoCommit(false);
                int[] batchStatus = preparedStatement.executeBatch();
                int savedRecords = 0;

                for (int ctr : batchStatus) {
                    if (ctr > 0) {
                        savedRecords++;
                    }
                }

                if (savedRecords == 0) {
                    log.info(parsedRecords + " records were parsed but no records were saved!!");
                    throw new NoDataFoundException("No records were saved from " + recipesDataFile.getName());
                }
            }
        } catch (IOException e) {
            log.error("I/O error while parsing recipes data file - " + e.getMessage());
            throw e;
        } catch (SQLException e) {
            log.error("SQL Exception while parsing recipes data file- " + e.getMessage());
            throw e;
        }
    }

    /**
     * Find recipe based on items and recipes table
     * @return a recipe
     * @throws SQLException thrown in case of an SQL exception
     */
    public static Recipe findRecipe() throws SQLException {
        Recipe recipe = null;

        try {
            // Query to find the recipe that matches the items in fridge
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT *\n" +
                                                                "FROM\n" +
                                                                "    RECIPES\n" +
                                                                "WHERE\n" +
                                                                "    name IN (SELECT \n" +
                                                                "            name\n" +
                                                                "        FROM\n" +
                                                                "            (SELECT \n" +
                                                                "                r.name, r.ingredient_count, i.useBy\n" +
                                                                "            FROM\n" +
                                                                "                recipes r\n" +
                                                                "            LEFT JOIN items i ON r.ingredient_name = i.name\n" +
                                                                "            WHERE\n" +
                                                                "                i.useBy > ?\n" +
                                                                "                    AND (i.amount - r.ingredient_amount) >= 0\n" +
                                                                "            ORDER BY r.name , useBy) AS T1\n" +
                                                                "        GROUP BY name\n" +
                                                                "        HAVING COUNT(*) >= ingredient_count\n" +
                                                                "        LIMIT 1);\n");

            preparedStatement.setTimestamp(1, new Timestamp(new java.util.Date().getTime()));

            ResultSet resultSet = preparedStatement.executeQuery();

            // If the resultSet is not empty, set the recipe object
            Recipe temp = new Recipe();
            ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();

            while (resultSet.next()) {
                // Set the recipe name the first time
                if (temp.getName() == null) { // The first time around it will be
                    temp.setName(resultSet.getString("name"));
                }

                Ingredient ingredient = new Ingredient();
                ingredient.setItem(resultSet.getString("ingredient_name"));
                ingredient.setAmount(resultSet.getInt("ingredient_amount"));
                ingredient.setUnit(Unit.valueOf(resultSet.getString("ingredient_unit")));

                ingredients.add(ingredient);
            }

            if (ingredients.size() > 0) {
                temp.setIngredients(ingredients);
            }

            resultSet.close();

            // If temp is not empty, set recipe with temp
            if (!temp.isEmpty()) {
                recipe = temp;
            }
        } catch (SQLException e) {
            log.error("SQL Exception while querying for recipes - " + e.getMessage());
            throw e;
        }

        return recipe;
    }

    /**
     * Reset DB
     * @throws SQLException is thrown in case of an exception
     */
    public static void resetDB() throws SQLException {
        try {
            Statement statement = connection.createStatement();

            // Delete all records
            statement.execute("DELETE FROM items;");
            statement.execute("DELETE FROM recipes;");
        } catch (SQLException e) {
            log.error("SQL Exception while resetting tables - " + e.getMessage());
            throw e;
        }
    }
}
