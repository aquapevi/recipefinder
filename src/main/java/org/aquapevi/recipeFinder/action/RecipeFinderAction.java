/*
 * Copyright 2006 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aquapevi.recipeFinder.action;

import com.fasterxml.jackson.core.JsonParseException;
import com.opensymphony.xwork2.ActionSupport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.util.List;

import com.opensymphony.xwork2.conversion.annotations.Conversion;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aquapevi.recipeFinder.exception.NoDataFoundException;
import org.aquapevi.recipeFinder.model.Item;
import org.aquapevi.recipeFinder.model.Recipe;
import org.aquapevi.recipeFinder.utils.Utils;

/**
 * Action to get the "items in fridge" and "recipes" data
 */
@Conversion()
public class RecipeFinderAction extends ActionSupport {

    public static final Log log = LogFactory.getLog(RecipeFinderAction.class);

    private File itemsInFridgeDataFile;
    private String itemsInFridgeDataFileContentType;
    private String itemsInFridgeDataFileFileName;

    private File recipesDataFile;
    private String recipesDataFileContentType;
    private String recipesDataFileFileName;

    private boolean takeOut = false;
    private Recipe selectedRecipe;

    public boolean isTakeOut() {
        return takeOut;
    }

    public Recipe getSelectedRecipe() {
        return selectedRecipe;
    }

    public File getItemsInFridgeDataFile() {
        return itemsInFridgeDataFile;
    }

    public void setItemsInFridgeDataFile(File itemsInFridgeDataFile) {
        this.itemsInFridgeDataFile = itemsInFridgeDataFile;
    }

    public String getItemsInFridgeDataFileContentType() {
        return itemsInFridgeDataFileContentType;
    }

    public void setItemsInFridgeDataFileContentType(String itemsInFridgeDataFileContentType) {
        this.itemsInFridgeDataFileContentType = itemsInFridgeDataFileContentType;
    }

    public String getItemsInFridgeDataFileFileName() {
        return itemsInFridgeDataFileFileName;
    }

    public void setItemsInFridgeDataFileFileName(String itemsInFridgeDataFileFileName) {
        this.itemsInFridgeDataFileFileName = itemsInFridgeDataFileFileName;
    }

    public File getRecipesDataFile() {
        return recipesDataFile;
    }

    public void setRecipesDataFile(File recipesDataFile) {
        this.recipesDataFile = recipesDataFile;
    }

    public String getRecipesDataFileContentType() {
        return recipesDataFileContentType;
    }

    public void setRecipesDataFileContentType(String recipesDataFileContentType) {
        this.recipesDataFileContentType = recipesDataFileContentType;
    }

    public String getRecipesDataFileFileName() {
        return recipesDataFileFileName;
    }

    public void setRecipesDataFileFileName(String recipesDataFileFileName) {
        this.recipesDataFileFileName = recipesDataFileFileName;
    }

    public String getDataFiles() throws Exception {
        Utils.resetDB();
        return INPUT;
    }

    public String getRecipe() throws Exception {
        if (itemsInFridgeDataFile == null) {
            addFieldError("itemsInFridgeDataFile", getText("error.upload.a.items.data.file"));
        }

        if (recipesDataFile == null) {
            addFieldError("recipesDataFile", getText("error.upload.a.recipes.data.file"));
        }

        // Preliminary check to make sure the data for the items in fridge is a 'CSV' file
        if (itemsInFridgeDataFileFileName != null && !itemsInFridgeDataFileFileName.toUpperCase().endsWith(".CSV")) {
            addFieldError("itemsInFridgeDataFile", getText("error.invalid.items.data.format"));
        }

        // Preliminary check to make sure the data for the items in fridge is a 'json' file
        if (recipesDataFileFileName != null && !recipesDataFileFileName.toUpperCase().endsWith(".JSON")) {
            addFieldError("recipesDataFile", getText("error.invalid.recipes.data.format"));
        }

        if (hasActionErrors() || hasFieldErrors()) {
            return INPUT;
        } else {
            List<Item> itemsInFridge = null;
            List<Recipe> recipes = null;

            try {
                // Parse item data CSV
                Utils.parseItemsDataFile(itemsInFridgeDataFile);
            } catch (FileNotFoundException e) {
                log.error("Items data file was not found - " + e.getMessage());
                addActionError("Application error - Please try again!");
            } catch (IOException e) {
                log.error("IO Exception while parsing Items data file - " + e.getMessage());
                addActionError("Application error - Please try again!");
            } catch (SQLException e) {
                log.error("SQL Exception while parsing Items data file - " + e.getMessage());
                addActionError("Application error - Please try again!");
            } catch (NoDataFoundException e) {
                log.error("No parsable data was found in the Items data file.");
                addFieldError("itemsInFridgeDataFile", getText("error.itemDataFile.no.data.found"));
            }

            try {
                // Parse recipes data
                Utils.parseRecipesDataFile(recipesDataFile);
            } catch (JsonParseException e) {
                log.error("Error parsing recipes data file - " + e.getMessage());
                addActionError("Recipes data file is invalid or has errors. Please fix it and try again.");
            } catch (IOException e) {
                log.error("IO Exception while parsing Items data file - " + e.getMessage());
                addActionError("Application error - Please try again!");
            } catch (NoDataFoundException e) {
                log.error("No parseable data was found in the Items data file.");
                addFieldError("itemsInFridgeDataFile", getText("error.itemDataFile.no.data.found"));
            } catch (SQLException e) {
                log.error("SQL Exception while parsing recipes data file was not found - " + e.getMessage());
                addActionError("Application error - Please try again!");
            }

            // So far so good! Now, find a recipe...
            try {
                selectedRecipe = Utils.findRecipe();

                if (selectedRecipe == null) {
                    // No recipes found, set takeOut to true
                    this.takeOut = true;
                }
            } catch (SQLException e) {
                log.error("SQL Exception while parsing querying for recipes - " + e.getMessage());
                addActionError("Application error!");
            }

            if (hasActionErrors()) {
                return ERROR;
            } else {
                return SUCCESS;
            }
        }
    }
}
