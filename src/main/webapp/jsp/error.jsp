<!DOCTYPE html PUBLIC 
	"-//W3C//DTD XHTML 1.1 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Error</title>
	<s:head />
</head>
<body>
    <div id="main">
        <!--<h3>Main Content</h3>-->
        <decorator:body/>
        <hr />
    </div>

    <div id="sub">
        <h3>Error</h3>
        <hr style="display: block"/>
    </div>

    <div id="nav">
        <div class="wrapper">
            <!--<h3>Nav. bar</h3>-->
            <ul class="clearfix">
                 <li class="last">
                     <s:url id="loadData" action="FindRecipe" method="getDataFiles"/>
                     <s:a href="%{loadData}"><s:text name="menu.loadData" /></s:a>
                 </li>
            </ul>
        </div>
        <hr />
    </div>

    <s:if test="hasActionErrors()">
        <div class="errors">
            <s:actionerror/>
        </div>
    </s:if>

</body>
</html>
