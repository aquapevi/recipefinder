<!DOCTYPE html PUBLIC 
	"-//W3C//DTD XHTML 1.1 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Recipe</title>
	<s:head />
</head>
<body>

    <div id="nav">
        <div class="wrapper">
            <!--<h3>Nav. bar</h3>-->
            <ul class="clearfix">
                 <li class="last">
                     <s:url id="loadData" action="FindRecipe" method="getDataFiles"/>
                     <s:a href="%{loadData}"><s:text name="menu.loadData" /></s:a>
                 </li>
            </ul>
        </div>
        <hr />
    </div>

    <s:if test="takeOut">
        <div id="sub">
            <h3>Order take out!</h3>
            <hr style="display: block"/>
        </div>
    </s:if>
    <s:else>
        <div id="sub">
            <h3>Recipe</h3>
            <hr style="display: block"/>
        </div>
        You should make <s:property value="selectedRecipe.name"/>.
        <br/><br/>
        Ingredients -
        <br/><br/>
        <ul>
            <s:iterator value="selectedRecipe.ingredients">
                <li><s:property value="amount"/> <s:property value="unit"/> <s:property value="item"/> </li>
            </s:iterator>
        </ul>

    </s:else>
</body>
</html>
