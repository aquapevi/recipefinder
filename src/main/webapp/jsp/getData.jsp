<!DOCTYPE html PUBLIC 
	"-//W3C//DTD XHTML 1.1 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Upload</title>
	<s:head />
</head>
<body>
	<s:form action="FindRecipe" method="POST" enctype="multipart/form-data">
        <div id="sub">
            <h3>Upload</h3>
            <hr style="display: block"/>
            <br/><br/>
            <s:actionerror/>
        </div>

		<s:file key="itemsInFridgeDataFile"
                tooltip="%{getText('tooltip.itemsInFridgeDataFile')}" required="true"
                accept="text/csv" />
		<s:file key="recipesDataFile"
                tooltip="%{getText('tooltip.recipesDataFile')}" required="true"
                accept="text/json"/>
		<s:submit key="upload" method="getRecipe"/>
	</s:form>
</body>
</html>
	